var j = jQuery;
var issues = j('.issuerow');
var output = '';
var people = {};
var total = 0;

issues.each(function(index, issue) {
    var $issue = j(issue);
    var person = j($issue.find('.assignee a')).text();
    var $progressImages = j($issue.find('.progress img'));
    var $progress;

    if (!person) {
        person = "Unassigned";
    }


    $progressImages.each(
        function(index, thing)
        {
            var title = j(thing).attr('title');

            if (title)
            {
                if (title.indexOf("Remaining") > -1)
                {
                    $progress = j(thing);
                }
            }

        }
    )

    if (!$progress) {
        return
    }

    var title = $progress.attr('title');
    if (!title) return;
    var time = title.split(' - ')[1];
    if (time == "Not Specified") {
        time = "0h"
    }

    time = parseFloat(time.slice(0, -1));

    var $person = j($issue.find('.assignee'));
    var $personSpan = j($person.find('.time'));

    if ($personSpan.length != 0) {
        $personSpan.text(time + 'h');
    } else {
        $person.append('<span class=\'time\'>' + time + 'h</span'); 
    }

    if (people[person]) {
        people[person] += time;
    } else {
        people[person] = time;
    }
});

for (var person in people) {
    total += people[person];
    output += person + ' : ' + people[person] + 'h\n';
}

output += '---------------------\n';
output += 'Total : ' + total + 'h\n';

alert(output);